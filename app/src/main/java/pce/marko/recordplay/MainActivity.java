package pce.marko.recordplay;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.nio.ShortBuffer;

import static android.R.attr.delay;

public class MainActivity extends AppCompatActivity {

    final int SAMPLE_RATE = 44100;
    boolean mShouldContinue = false;

    Button rec, stop;

    short[] audioBuffer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         rec = (Button)findViewById(R.id.record);
         stop = (Button)findViewById(R.id.stop);

        rec.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mShouldContinue = true;
                recordAudio();
                try {
                    playAudio();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mShouldContinue = false;
            }
        });
    }

    private void recordAudio(){
        Log.d("Petlje ", "recordAudio() IN");

         new Thread(new Runnable() {
            @Override
            public void run() {

                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);

                 int bufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
                 // Log.d(this.toString() + "BUFFER SIZE: ", String.valueOf(bufferSize));

                // AKO SE DOGODILA GRESKA KOD ALOCIRANJA VELICINE BUFFERA, TJ. BUFFER NIJE ALOCIRAN, TADA RUCNO NAPRAVI BUFFER 2X VECI OD SAMPLE RATE-A !?
                if( (bufferSize == AudioRecord.ERROR) || (bufferSize == AudioRecord.ERROR_BAD_VALUE) ){
                // if( (bufferSize != AudioRecord.ERROR) && (bufferSize != AudioRecord.ERROR_BAD_VALUE) ){
                    bufferSize = SAMPLE_RATE * 2; // OVIME SE GARANTIRA AKO SE DOGODI GRESKA, DA SE SIGURNO DEFINIRA VELICINA BUFFERA
                }
                else { // (0 || 0) != 0) -> 0 != 0 -> NIJE ISTINA PA ULAZI OVDJE, TJ. NEMA GRESKE PRI ALOKACIJI BUFFERA, BUFFER HE ALOCIRAN!
                    Log.d(this.toString() + "bufferMinSize", String.valueOf(bufferSize));
                    // Log.e(this.toString(), "ERROR 1");
                }

                audioBuffer = new short[bufferSize/2];

                AudioRecord record = new AudioRecord(MediaRecorder.AudioSource.DEFAULT, SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize);
                // AudioRecord record = new AudioRecord(MediaRecorder.AudioSource.DEFAULT, SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_8BIT, bufferSize);
                Log.d("PETLJE", String.valueOf(record.getAudioFormat()) + ":" + String.valueOf(record.getAudioSource() + ":" + String.valueOf(record.getSampleRate() + ":" + String.valueOf(record.getChannelCount()))));

                // testRecorderPlay();

               if(record.getState() != AudioRecord.STATE_INITIALIZED){
                    Log.e("Petljee", "Audio Record can't initialize!");
                    return;
                }
                else{
                   Log.e("Petljee", "Audio Record  initialize!");
               }

               record.startRecording();

                Log.v("Petlje", "Start recording");

                long shortsRead = 0;
                  while(mShouldContinue){
                      Log.v("REC Start While", "while recording");
                      int numberOfShorts = record.read(audioBuffer, 0, audioBuffer.length);
                      shortsRead += numberOfShorts;

                  }

                Log.d("PETLJE", "stop release record");
                record.stop();
                record.release();

                Log.v("Petlje", String.format("Recording stopped. Samples read: %d", shortsRead));

            }
        }).start();
        Log.d("Petlje ", "recordAudio() OUT");
    }

    ShortBuffer mSamples;
    int mNumSamples;
    private void playAudio() throws InterruptedException {

        Log.d("Petlje ", "playAudio() IN");

       Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                int bufferSize = AudioTrack.getMinBufferSize(SAMPLE_RATE, AudioFormat.CHANNEL_OUT_MONO,  AudioFormat.ENCODING_PCM_16BIT);

                if (bufferSize == AudioTrack.ERROR || bufferSize == AudioTrack.ERROR_BAD_VALUE) {
                    bufferSize = SAMPLE_RATE * 2;
                }

                AudioTrack audioTrack = new AudioTrack(
                        AudioManager.STREAM_MUSIC,
                        SAMPLE_RATE,
                        AudioFormat.CHANNEL_OUT_MONO,
                        AudioFormat.ENCODING_PCM_16BIT,
                        bufferSize,
                        AudioTrack.MODE_STREAM);

                audioTrack.play();

                Log.v("Petlje", "Audio streaming started");


                long shortsRead = 0;
                while(mShouldContinue){
                    try {
                        int numberOfShort = audioTrack.write(audioBuffer, 0, audioBuffer.length);
                        shortsRead += numberOfShort;
                    }
                    catch (Exception exp){
                        Log.e("Petlje err ", exp.getMessage());
                    }


                }


                if(!mShouldContinue){
                    audioTrack.release();
                }

                Log.v("Petlje ", "Audio streaming finished. Samples written: " + shortsRead);

            }
        });
        thread.sleep(1000);
        thread.start();
        // THREAD UNUTAR playAudio() FUNKCIJE KAD SE POZOVE I POKRECE SE U OS-U JVM, TE KAD KRENE SA RADOM ZAUZIMA SVOJE MJESTO U OS-U,
        // SVOJU MEMORIJU I OSTALO, TE SE VRACA SVOM POZIVATELJU playAudio() FUNKCIJI, ALI THREAD DALJE PARALELNO RADI, A playAudio()
        // FUNKCIJA NASTAVLJA DALJE SA RADOM TE SE VRACA U MainActivity GDJE JE POZVANA I Main IDE IDALJE I POZIVA recordAudio()
        // FUNKCIJU KOJA CE POZVATI I NAPRAVITI NOVI THREAD NA IZVRSAVANJE U JVM PARALELNO SA UI THREAD-OM, playAudio() THREAD-OM,
        // TE CE SE TAJ THREAD KAD SE FORMIRA I POKRENE U JVM VRATITI U FUNKCIJU playAudio() KOJA NASTAVLJA DALJE I VRACA SE U MAIN.
        // KAD THREAD ZAPOCNE SA SVOJIM RADOM SA .Start() U OS-U JVM, VRACA SE return ODMAH SVOM POZIVATELJU, I THREAD RADI U OS-U, A POZIVATELJ NASTAVLJ DALJE
        Log.d("Petlje ", "playAudio() OUT");
    }

    private void testRecordPlay(){
         for (int rate : new int[]{8000, 11025, 22050, 44100}) {
                    for (short audioFormat : new short[] { AudioFormat.ENCODING_PCM_8BIT, AudioFormat.ENCODING_PCM_16BIT }) {
                        for (short channelConfig : new short[] { AudioFormat.CHANNEL_IN_MONO, AudioFormat.CHANNEL_IN_STEREO }) {


                            try {
                                int bufferSizeTEMP = AudioRecord.getMinBufferSize(rate, channelConfig, audioFormat);

                                if (bufferSizeTEMP != AudioRecord.ERROR_BAD_VALUE) {
                                    AudioRecord recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, rate, channelConfig, audioFormat, bufferSizeTEMP);
                                    Log.d("PETLJE ", String.valueOf(rate) + " : " + String.valueOf(audioFormat) + " : " + String.valueOf(channelConfig));

                                    // if (recorder.getState() == AudioRecord.STATE_INITIALIZED)
                                       // return recorder;
                                }
                            } catch (Exception e) {
                                Log.e(this.toString(), rate + "Exception, keep trying.",e);
                            }
                        }
                    }
                }
    }


}
